<?php

namespace App\Helpers;


use App\Models\ReferrerBonus;
use App\Models\User;
use App\Models\UserBalance;
use Illuminate\Database\Eloquent\Collection;

class PaymentHelper
{
	public static function payment($userId, $amount)
	{
		$referrerBonuses = ReferrerBonus::all()->pluck('percent', 'level');

		/** @var Collection $parents */
		$parents = User::withDepth()->ancestorsAndSelf($userId)->sortByDesc('depth');

		/** @var User $user */
		$user = $parents->shift();

		$baseDepth = $user->depth;

		$parents->each(function (User $item) use ($baseDepth, $userId, $amount, $referrerBonuses) {
			$level = $baseDepth - $item->depth;
			if ($percent = $referrerBonuses->get($level))
			{
				UserBalance::create([
					'user_id' => $item->id,
					'amount'  => round($amount * $percent / 100, 2),
					'comment' => 'referral id:' . $userId
				]);
			}

		});

		UserBalance::create([
			'user_id' => $userId,
			'amount'  => $amount,
			'comment' => 'Adding founds',
		]);
	}
}