<?php

namespace App\Http\Controllers;


use App\Helpers\PaymentHelper;
use App\Http\Requests\PaymentRequest;
use App\Models\ReferrerBonus;
use App\Models\User;

class PaymentController extends Controller
{
	public function payForm()
	{
		return view('pay-form', [
			'users' => User::all()->sortBy('id'),
		]);
	}

	public function pay(PaymentRequest $request)
	{
		PaymentHelper::payment(
			$request->get('user_id'),
			$request->get('amount')
		);

		return redirect()->back();
	}

	public function userTree()
	{
		$nodes = User::get()->toTree();

//		$traverse = function ($categories, $prefix = '-') use (&$traverse) {
//			foreach ($categories as $category) {
//				echo PHP_EOL.$prefix.' '.$category->name;
//
//				$traverse($category->children, $prefix.'-');
//			}
//		};
//
//		$traverse($nodes);
//
		return view('user-tree', ['nodes' => $nodes]);
	}
}