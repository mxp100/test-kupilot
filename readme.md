## INSTALLATION
- `composer install`
- `php artisan migrate`
- `php artisan db:seed` - generate 100 users with random referring

## REFERRER URL
?referrer=[user_id]

## SETTINGS
In table referrer_bonuses (infinity level) percent settings by level

In config/referrer.php additional settings

## STARTING
`php artisan server` - URL: localhost:8000
