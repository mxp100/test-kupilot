<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferrerBonus
 *
 * @property integer $id
 * @property integer $level
 * @property integer $percent
 * @property Carbon $updated_at
 * @property Carbon $created_at
 * @package App\Models
 */
class ReferrerBonus extends Model
{
    protected $fillable = [
    	'level',
	    'percent',
    ];
}
