<?php

namespace App\Observers;


use App\Models\User;
use App\Models\UserBalance;
use Illuminate\Support\Facades\DB;

class UserBalanceObserver
{
	public function saved(UserBalance $userBalance)
	{
		User::where('id', $userBalance->user_id)->increment('balance', $userBalance->amount);
	}
}