<?php

use App\Models\ReferrerBonus;
use Illuminate\Database\Seeder;

class ReferrerBonusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    ReferrerBonus::create([
		    'level' => 1,
		    'percent' => 5,
	    ]);
	    ReferrerBonus::create([
		    'level' => 2,
		    'percent' => 3,
	    ]);
	    ReferrerBonus::create([
		    'level' => 3,
		    'percent' => 2,
	    ]);
	    ReferrerBonus::create([
		    'level' => 4,
		    'percent' => 1,
	    ]);
    }
}
