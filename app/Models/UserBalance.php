<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserBalance
 *
 * @property integer $id
 * @property double  $amount
 * @property string  $comment
 * @property Carbon  $updated_at
 * @property Carbon  $created_at
 * @package App\Models
 */
class UserBalance extends Model
{
	protected $fillable = [
		'user_id',
		'amount',
		'comment',
	];


}
