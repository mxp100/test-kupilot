<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

/**
 * Class Referrer
 * Middleware for setup cookie and fix session for referrer
 *
 * @package App\Http\Middleware
 */
class Referrer
{
	public function handle(Request $request, Closure $next)
	{
		if ($request->hasCookie(config('referrer.cookie.name'))
			&& !($request->has(config('referrer.request.name')) && config('referrer.rewrite_empty')))
		{
			$referrerId = $request->cookie(config('referrer.cookie.name'));
			session([
				'referrer_id' => $referrerId
			]);
		}
		elseif ($request->has(config('referrer.request.name')))
		{
			$referrerId = $request->get(config('referrer.request.name'));

			session([
				'referrer_id' => $referrerId
			]);

			Cookie::queue(config('referrer.cookie.name'), $referrerId, config('referrer.cookie.lifetime'));
		}
		elseif (!config('referrer.rewrite_empty'))
		{
			Cookie::queue(config('referrer.cookie.name'), 0, config('referrer.cookie.lifetime'));
			session([
				'referrer_id' => 0
			]);
		}

		return $next($request);
	}
}