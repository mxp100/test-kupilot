<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();

		$userIds = [0];

		foreach (range(1, 100) as $index)
		{
			$user = User::create([
				'name'     => $faker->name,
				'email'    => $faker->email,
				'password' => bcrypt('secret'),
				'parent_id' => array_random($userIds),
			]);

			$userIds[] = $user->id;
		}

	}
}
