<ul>
    @foreach($nodes as $node)
        <li>#{{ $node->id }} {{ $node->name }}: {{ $node->balance }}$
            @includeWhen($node->children, 'user-tree.node', ['nodes' => $node->children])
        </li>
    @endforeach
</ul>