<?php

return [
	'rewrite_empty' => true,
	'request' => [
		'name' => 'referrer',
	],
	'cookie'  => [
		'name'     => 'referrer',
		'lifetime' => 43200,
	],
];